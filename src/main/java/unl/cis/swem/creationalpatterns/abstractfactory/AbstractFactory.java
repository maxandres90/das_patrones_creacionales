/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package unl.cis.swem.creationalpatterns.abstractfactory;
/**
 *
 * @author wduck
 * @param <T>
 */
public interface AbstractFactory <T>{
    
   T create(String tipo);
   
}
