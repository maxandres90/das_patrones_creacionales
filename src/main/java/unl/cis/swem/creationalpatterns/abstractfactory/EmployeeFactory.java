/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package unl.cis.swem.creationalpatterns.abstractfactory;

import java.time.LocalDate;
import java.time.Month;
import unl.cis.swem.creationalpatterns.domain.AdministrativePersonal;
import unl.cis.swem.creationalpatterns.domain.Employee;
import unl.cis.swem.creationalpatterns.domain.Person;
import unl.cis.swem.creationalpatterns.domain.SocialEconomicLevel;
import unl.cis.swem.creationalpatterns.domain.Student;
import unl.cis.swem.creationalpatterns.domain.Teacher;


/**
 *
 * @author wduck
 */
public abstract class EmployeeFactory implements AbstractFactory<Employee>{
    
    @Override
    public Employee create(String tipo_persona){
        switch (tipo_persona) {
            case "TEACHER":
                return new Teacher("Corazón", "Barriga", "1134608674", LocalDate.of(1978, Month.MARCH, 27), "Cardiológo y Gastroenterólogo");
            case "ADMIN":
                return new AdministrativePersonal("Kimberly", "Flores del Jardín", "110456980", LocalDate.of(2000, Month.MARCH, 24), "Director");
            default:
                return null;
        }
    }
    
}
