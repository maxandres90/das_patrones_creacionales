/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package unl.cis.swem.creationalpatterns.domain;

import java.time.LocalDate;

/**
 *
 * @author wduck
 */
public abstract class Employee extends Person{


    public Employee(String firstName, String lastName, String dui) {
        super(firstName, lastName, dui);
    }

    public Employee(String firstName, String lastName, String dui, LocalDate birthDate) {
        super(firstName, lastName, dui, birthDate);
    }
    
//    @Override
//    public void task() {
//        System.out.println("SOY UN: " + this.getClass().getCanonicalName().toUpperCase() + 
//            " Y SOY UN EMPLEADO");
//    }

}
