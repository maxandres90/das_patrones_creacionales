/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package unl.cis.swem.creationalpatterns.view;

import java.util.Scanner;
import unl.cis.swem.creationalpatterns.abstractfactory.AbstractFactory;
import unl.cis.swem.creationalpatterns.abstractfactory.EmployeeFactory;
import unl.cis.swem.creationalpatterns.domain.Person;
import unl.cis.swem.creationalpatterns.factorymethod.PersonFactoryMethod;
import unl.cis.swem.creationalpatterns.factorymethod.StudentFactory;

/**
 *
 * @author wduck
 */
public class AbstractFactoryMain {

    public static void main(String[] args) {
       
        PersonFactoryMethod personFactory=null;
        Scanner scanner = new Scanner(System.in);
        String entrada = "";
        
        System.out.println();
        System.out.println("--------------------------------------------------------------------");
        System.out.println("-------------------- Grupo 05 AbstractFactory ----------------------");
        
        do{
            
            System.out.println("--------------------------------------------------------------------");
            System.out.println("----- Patrones disponibles: ----------------------------------------");
            System.out.println("----- (AbstractFactory) EmployeeFactory (AF)  ----------------------");
            System.out.println("----- (MethodFactory) StudentFactory (MF) --------------------------");
            System.out.println("--------------------------------------------------------------------");
            
            System.out.println();
            System.out.print("Ingrese la fábrica a elegir: ");
            entrada = scanner.nextLine();
            
            AbstractFactory fabricaAF = null;
            PersonFactoryMethod fabricaMF = null;
            
            Person object = null;
            
            if (entrada.equals("AF")){
                System.out.println("--------------------------------------------------------------------");
                System.out.println("----- Clases concretas: --------------------------------------------");
                System.out.println();
                System.out.println("----- Docente (TEACHER)  -------------------------------------------");
                System.out.println("------Personal Administrativo (ADMIN) ------------------------------");
                System.out.println();
                System.out.print("Ingrese la clase concreta a elegir: ");
                entrada = scanner.nextLine();
                System.out.println();
                
                fabricaAF = new EmployeeFactory() {};
                object = (Person) fabricaAF.create(entrada+"");
            }else if (entrada.equals("MF")){
                fabricaMF = new StudentFactory();
                object = fabricaMF.create();
            }
                        
            System.out.println("===>>> toString(): " + object.toString());
            System.out.println("===>>> fullName(): " + object.fullName());
            System.out.print("===>>> task(): ");
            object.task();
            System.out.println();
            System.out.print("Desea realizar otra iteración SI(y) NO(n): ");

            scanner = new Scanner(System.in);
            entrada = scanner.nextLine();
            
        }while((entrada+"").equals("y"));
        
    }
}
