/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package unl.cis.swem.creationalpatterns.view;

import java.util.Scanner;
import unl.cis.swem.creationalpatterns.domain.Person;
import unl.cis.swem.creationalpatterns.factorymethod.AdministrativePersonalFactory;
import unl.cis.swem.creationalpatterns.factorymethod.PersonFactoryMethod;
import unl.cis.swem.creationalpatterns.factorymethod.StudentFactory;
import unl.cis.swem.creationalpatterns.factorymethod.TeacherFactory;

/**
 *
 * @author wduck
 */
public class FactoryMethodMain {

    public static void main(String[] args) {
       
        PersonFactoryMethod personFactory=null;
        Scanner scanner = new Scanner(System.in);
        String entrada = "";
        
        System.out.println();
        System.out.println("--------------------------------------------------------------------");
        System.out.println("--------------------- Grupo 05 FactoryMethod -----------------------");
        System.out.println("--------------------------------------------------------------------");
        System.out.println("----- Opciones: ----------------------------------------------------");
        System.out.println("----- 1. Docente ---------------------------------------------------");
        System.out.println("----- 2. Estudiante ------------------------------------------------");
        System.out.println("----- 3. PersonalAdministrativo ------------------------------------");
        System.out.println("--------------------------------------------------------------------");
        
        do{
            System.out.println();
            System.out.print("Ingrese un valor: ");
            entrada = scanner.nextLine();
            System.out.println();

            switch (Integer.parseInt(entrada+"")) {
                case 1:
                    personFactory = new TeacherFactory();
                    break;
                case 2:
                    personFactory = new StudentFactory();
                    break;
                case 3:
                    personFactory = new AdministrativePersonalFactory();
                    break;
                default:
                    System.out.println("Entrada inválida");
                    throw new AbstractMethodError("ERRROR");
            }

            Person object = personFactory.create();
            
            if (object!=null){   
                System.out.println("===>>> toString(): " + object.toString());
                System.out.println("===>>> fullName(): " + object.fullName());
                System.out.print("===>>> task(): ");
                object.task();
            }else{
                System.out.println("===>> OPCIÓN INVÁLIDA <<==");
            }
            
            System.out.println();
            System.out.print("Desea realizar otra iteración SI(y) NO(n): ");

            scanner = new Scanner(System.in);
            entrada = scanner.nextLine();
            
        }while((entrada+"").equalsIgnoreCase("y"));
        
    }
}
